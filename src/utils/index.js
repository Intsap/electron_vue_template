const formatNumber = n => {
  const str = n.toString()
  return str[1] ? str : `0${str}`
}

export const formatTime = date => {
  const year = date.getFullYear()
  const month = date.getMonth() + 1
  const day = date.getDate()

  const hour = date.getHours()
  const minute = date.getMinutes()
  const second = date.getSeconds()

  const t1 = [year, month, day].map(formatNumber).join('/')
  const t2 = [hour, minute, second].map(formatNumber).join(':')

  return `${t1} ${t2}`
}

export const formatDate = (date, joinStr = '/') => {
  const year = date.getFullYear()
  const month = date.getMonth() + 1
  const day = date.getDate()

  return [year, month, day].map(formatNumber).join(joinStr)
}

// 获取当前月中第某一天的日期
export const getMonthStartDay = (date, d = '01') => {
  const t = new Date(date.getTime())
  return new Date(`${t.getFullYear()}-${setT(t.getMonth() + 1)}-${d}`)
}
/**
 * 获取当天所在月的最后一天日期，和下一个月的第一天日期
 * 调用getMonthDay(d), d默认值为01，d为要设置成的day；
 */
export const getMonthEndDay = date => {
  const curDate = new Date(date.getTime())
  const curMonth = curDate.getMonth()
  /*  生成实际的月份: 由于curMonth会比实际月份小1, 故需加1 */
  curDate.setMonth(curMonth + 1)
  /* 将日期设置为0, 返回当月最后一天日期， 将日期设置为1，返回下一个月第一天日期 */
  curDate.setDate(0)
  /* 返回当月的天数 */
  return curDate
}

const setT = e => {
  return `${e}`.length > 1 ? `${e}` : `0${e}`
}

/**
 * 获取当天所在周的周一和周日的日期
 * 返回 YYYY-MM-DD 格式时间
 */
const setToday = function (t = new Date()) {
  return `${t.getFullYear()}-${setT(t.getMonth() + 1)}-${setT(t.getDate())}`
}
export const getWeekStartDay = date => {
  const curDate = new Date(date.getTime())
  return new Date(setToday(new Date(curDate.setDate(curDate.getDate() - curDate.getDay() + 1))))
}
export const getWeekEndDay = date => {
  const curDate = new Date(date.getTime())
  return new Date(setToday(new Date(curDate.setDate(curDate.getDate() + (7 - curDate.getDay())))))
}

export const lastMonthFirst = date => {
  const time = new Date(date.getTime())
  let year = time.getFullYear()
  let month = time.getMonth()
  if (month === 0) {
    month = 12
    year = year - 1
  }
  if (month < 10) {
    month = '0' + month
  }
  return new Date(year + '-' + month + '-' + '0' + 1)
}
// 获取上个月最后一天
export const lastMonthLast = date => {
  const time = new Date(date.getTime())
  let year = time.getFullYear()
  let month = time.getMonth()
  if (month === 0) {
    month = 12
    year = year - 1
  }
  if (month < 10) {
    month = '0' + month
  }

  const lastday = new Date(year, month, 0).getDate()
  return new Date(year + '-' + month + '-' + lastday)
}

export const nextMonthFirst = date => {
  const time = new Date(date.getTime())
  let year = time.getFullYear()
  let month = time.getMonth() + 2
  if (month === 13) {
    month = 1
    year = year + 1
  } else if (month === 14) {
    month = 2
    year = year + 1
  }
  if (month < 10) {
    month = '0' + month
  }
  return new Date(year + '-' + month + '-' + '0' + 1)
}
// 获取上个月最后一天
export const nextMonthLast = date => {
  const time = new Date(date.getTime())
  let year = time.getFullYear()
  let month = time.getMonth() + 2
  if (month === 13) {
    month = 1
    year = year + 1
  } else if (month === 14) {
    month = 2
    year = year + 1
  }
  if (month < 10) {
    month = '0' + month
  }

  const lastday = new Date(year, month, 0).getDate()
  return new Date(year + '-' + month + '-' + lastday)
}

export const trim = str => {
  if (str == null || typeof str === 'undefined') {
    return ''
  }
  return str.replace(/(^\s*)|(\s*$)/g, '')
}

export const isNullObj = object => {
  return object == null || typeof object === 'undefined'
}

export const isEmptyStr = str => {
  return str == null || typeof str === 'undefined' || str === ''
}

export const isBlankStr = str => {
  return str == null || typeof str === 'undefined' || str === '' || trim(str) === ''
}
