// import stringutil from './stringutil'
// import store from '../store/index'

// 前置拼接url
const api = 'http://192.168.31.98:8080/'

// 处理promise和fetch的兼容性以及引入
// require('es6-promise').polyfill();
// require('isomorphic-fetch');
const [id, token, source] = [null, '', 3];

// 处理get请求，传入参数对象拼接
const formatUrl = obj => {
  const params = Object.values(obj).reduce((a, b, i) => `${a}${Object.keys(obj)[i]}=${b}&`, '?');
  return params.substring(0, params.length - 1);
};

const codeMessage = {
  400: '发出的请求有错误',
  401: '用户没有权限',
  403: '用户得到授权，但访问是被禁止的',
  405: '请求方式错误',
  404: '发出的请求不存在',
  406: '请求的格式不可得',
  410: '请求的资源被永久删除，且不会再得到的',
  422: '当创建一个对象时，发生一个验证错误',
  500: '服务器发生错误，请检查服务器',
  502: '网关错误',
  503: '服务不可用',
  504: '网关超时'
}

const Fetch = (url, option = {}) => {
  // if (!stringutil.isBlank(store.state.user)) {
  //   const user = store.state.user
  //   id = user.id
  //   token = user.token
  // }
  option.headers = option.headers || {};

  option.headers = {
    ...option.headers,
    id,
    token,
    source
  };
  const m = (option.method || '').toLocaleLowerCase();
  if (m === 'get') {
    if (option.query) {
      url = url + formatUrl(option.query);
    }
  }
  // 对非get类请求头和请求体做处理
  if (m === 'post' || m === 'put' || m === 'delete') {
    // option.headers['Content-Type'] = option.headers['Content-Type'] || 'application/json';
    //  上传文件 header Content-Type 不设置 其余为json
    if (option.headers.noType) {} else {
      option.headers['Content-Type'] = 'application/json'
      option.body = JSON.stringify(option.body);
    }
  }
  return new Promise((resolve, reject) => {
    fetch(api + url, option)
      .then(response => {
        // eslint-disable-next-line no-global-assign
        status = response.status;
        if (response.status >= 401) {
          const errMsg = {
            errorCode: -1,
            errorMessage: codeMessage[response.status]
          }
          resolve(
            errMsg
          )
        }
        return response;
      })
      .then(parseJSON)
      .then(response => {
        response.status = status;
      })
      .catch(error => {
        console.log('err', error);
        Fetch.otherError && Fetch.otherError(error.message);
        const errMsg = {
          errorCode: -1,
          errorMessage: '网络异常请检查网络'
        }
        resolve(
          errMsg
        )
      });
  });
};

// response 转化
function parseJSON(response) {
  return response.json();
}

export default Fetch;
