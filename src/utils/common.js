export const repeat = (str = '0', times) => new Array(times + 1).join(str);
// 时间前面 +0
export const pad = (num, maxLength = 2) =>
  repeat('0', maxLength - num.toString().length) + num;
/** 时间格式的转换 */
export const formatTime = time =>
  `${pad(time.getHours())}:${pad(time.getMinutes())}:${pad(
    time.getSeconds()
  )}.${pad(time.getMilliseconds(), 3)}`;

//

/**
 * 分钟转换为 分：秒
 * @param s 秒
 */
export const formatTimingTime = (s) => {
  // 计算分钟
  // 算法：将秒数除以60，然后下舍入，既得到分钟数
  var h;
  h = Math.floor(s / 60);
  // 计算秒
  // 算法：取得秒%60的余数，既得到秒数
  s = s % 60;
  // 将变量转换为字符串
  h += '';
  s += '';
  // 如果只有一位数，前面增加一个0
  s = (s.length === 1) ? '0' + s : s;
  return h + ':' + s;
}

export const globalData = {}; // 全局公共变量

/**
 * 校验手机号是否正确
 * @param phone 手机号
 */

// export const verifyPhone = phone => {
//   const reg = /^1[34578][0-9]{9}$/;
//   const _phone = phone.toString().trim();
//   let toastStr =
//     _phone === ""
//       ? "手机号不能为空~"
//       : !reg.test(_phone) && "请输入正确手机号~";
//   return {
//     errMsg: toastStr,
//     done: !toastStr,
//     value: _phone
//   };
// };

// export const verifyStr = (str, text) => {
//   const _str = str.toString().trim();
//   const toastStr = _str.length ? false : `请填写${text}～`;
//   return {
//     errMsg: toastStr,
//     done: !toastStr,
//     value: _str
//   };
// };

/**
 *  手机号码中间加密
 * @param phone 要计算的值
 * @return 计算结果
 */
export function encryptPhone(phone) {
  const reg = /(\d{3})\d{4}(\d{4})/;
  return phone.replace(reg, '$1****$2');
}

/* 数组对象的深拷贝 */
export function objCopy(obj) {
  if (typeof obj !== 'object') {
    return;
  }
  return JSON.parse(JSON.stringify(obj));
}

/**
 *  属性名：属性值 数组合并为对象
 * @param {colunms | data} 属性名称数组 属性值数组
 * @return Arr 对象数组
 */

export function dataParser(colunms, data) {
  const res = [];
  data.forEach((item, index) => {
    res[index] = colunms.reduce((p, c, i) => {
      p[c] = item[i];
      return p;
    }, {});
  });
  return res;
}

/**
 *  js计算
 * @param {arg1 | arg2} 要计算的值
 * @return 计算结果
 */
// 加法
export function accAdd(arg1, arg2) {
  var r1, r2, m, n;
  try {
    r1 = arg1.toString().split('.')[1].length
  } catch (e) {
    r1 = 0
  }
  try {
    r2 = arg2.toString().split('.')[1].length
  } catch (e) {
    r2 = 0
  }
  m = Math.pow(10, Math.max(r1, r2))
  n = (r1 >= r2) ? r1 : r2;
  return parseFloat(((arg1 * m + arg2 * m) / m).toFixed(n));
}

export function getAccSub(arg1, arg2) {
  // 减法
  let r1, r2, m, n;
  try {
    r1 = arg1.toString().split('.')[1].length;
  } catch (e) {
    r1 = 0;
  }
  try {
    r2 = arg2.toString().split('.')[1].length;
  } catch (e) {
    r2 = 0;
  }
  // eslint-disable-next-line prefer-const
  m = Math.pow(10, Math.max(r1, r2));
  // eslint-disable-next-line prefer-const
  n = r1 >= r2 ? r1 : r2;
  return ((arg1 * m - arg2 * m) / m).toFixed(n);
}

export function getAccMul(arg1, arg2) {
  // 乘法
  let m = 0;
  const s1 = arg1.toString();
  const s2 = arg2.toString();
  try {
    m += s1.split('.')[1].length;
  } catch (e) {}
  try {
    m += s2.split('.')[1].length;
  } catch (e) {}
  return (
    (Number(s1.replace('.', '')) * Number(s2.replace('.', ''))) /
    Math.pow(10, m)
  );
}

// export function getAccDiv(arg1, arg2) {
//   "user strict";
//   // 除法
//   let [t1, t2, r1, r2] = [0, 0, 0, 0];

//   try {
//     t1 = arg1.toString().split(".")[1].length;
//   } catch (e) {}
//   try {
//     t2 = arg2.toString().split(".")[1].length;
//   } catch (e) {}
//   with (Math) {
//     r1 = Number(arg1.toString().replace(".", ""));
//     r2 = Number(arg2.toString().replace(".", ""));
//     return (r1 / r2) * pow(10, t2 - t1);
//   }
// }
