/**
 *  js计算
 * @param {arg1 | arg2} 要计算的值
 * @return 计算结果
 */
export default class M {
  _add(arg1, arg2) {
    // 加法
    let r1, r2, m;
    try {
      r1 = arg1.toString().split(".")[1].length;
    } catch (e) {
      r1 = 0;
    }
    try {
      r2 = arg2.toString().split(".")[1].length;
    } catch (e) {
      r2 = 0;
    }
    // m = Math.pow(10, Math.max(r1, r2));
    m = 1000;
    return (arg1 * m + arg2 * m) / m;
  }

  _sub(arg1, arg2) {
    // 减法
    let r1, r2, m, n;
    try {
      r1 = arg1.toString().split(".")[1].length;
    } catch (e) {
      r1 = 0;
    }
    try {
      r2 = arg2.toString().split(".")[1].length;
    } catch (e) {
      r2 = 0;
    }
    // m = Math.pow(10, Math.max(r1, r2));
    m = 1000;
    n = r1 >= r2 ? r1 : r2;
    let res = ((arg1 * m - arg2 * m) / m).toFixed(n);
    return parseFloat(res);
  }

  _mul(arg1, arg2) {
    // 乘法
    let m = 0,
      s1 = arg1.toString(),
      s2 = arg2.toString();
    try {
      m += s1.split(".")[1].length;
    } catch (e) {}
    try {
      m += s2.split(".")[1].length;
    } catch (e) {}
    return (
      (Number(s1.replace(".", "")) * Number(s2.replace(".", ""))) /
      Math.pow(10, m)
    );
  }
}
