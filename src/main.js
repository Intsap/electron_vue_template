import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import Fetch from './utils/request';
import stringutil from './utils/stringutil'

Vue.config.productionTip = false

Vue.prototype.Fetch = Fetch;
Vue.prototype.$isBlank = stringutil.isBlank;

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
