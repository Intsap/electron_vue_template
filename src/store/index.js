import Vue from 'vue'
import Vuex from 'vuex'

const VUEX_MUTATION = 'VUEX_MUTATION';
const mainGlobalStore = 'MAIN_GLOBAL_STORE'
const {
  ipcRenderer,
  remote
} = require('electron');

// 多进程state同步
const registerVuexNode = (store) => {
  if (process.type === 'browser') {
    global[mainGlobalStore] = store
  } else if (process.type === 'renderer') {
    const {
      getGlobal
    } = require('electron').remote
    // sync main process state while renderer process launching
    const mainState = remote.getGlobal(mainGlobalStore).state
    store.replaceState(JSON.parse(JSON.stringify(mainState)))
    store.subscribe((mutation, state) => {
      if (!mutation.payload || !mutation.payload.__IPC_MUTATION) {
        ipcRenderer.send(VUEX_MUTATION, mutation);
      }
      getGlobal(mainGlobalStore).replaceState(JSON.parse(JSON.stringify(state)))
    });

    ipcRenderer.on(VUEX_MUTATION, (event, mutation) => {
      store.commit(mutation.type, {
        ...mutation.payload || [],
        __IPC_MUTATION: true
      });
    });
  }
}

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    appName: 'Vein Mocap'
  },
  mutations: {
    setVal(state, {
      valName,
      val
    }) {
      state[valName] = val
    },
    setVals(state, values = {}) {
      Object.keys(values).forEach(key => {
        state[key] = values[key]
      })
    }
  },
  actions: {
    setVal({
      commit
    }, {
      valName,
      val
    }) {
      commit('setVal', {
        valName,
        val
      })
    },
    setVals(
      //
      {
        commit
      },
      values
    ) {
      commit('setVals',
        values
      )
    }
  },
  modules: {},
  plugins: [registerVuexNode],
  strict: process.env.NODE_ENV !== 'production'
})
