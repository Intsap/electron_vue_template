module.exports = {
  publicPath: './',
  pluginOptions: {
    electronBuilder: {
      nodeIntegration: true,
      builderOptions: {
        appId: 'com.appName.app',
        productName: 'appName', // 项目名，也是生成的安装文件名，即aDemo.exe
        copyright: 'Copyright © 2020', // 版权信息
        win: {
          icon: './public/app.ico'
        },
        mac: {
          icon: './public/app.png'
        }
      }
    }
  },
  css: {
    loaderOptions: {
      stylus: {
        import: '~@/assets/public.styl'
      }
    }
  },
  transpileDependencies: [
    'resize-detector'
  ]
}
